# Wardrobify

Team:

* Sarah Behrens - Shoes
* Jamie Schuetter- Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
The Wardrobe microservice had the models for the bins to store the shoes ina closet. 
In the shoe microservice I created a BinVO model to represent the bins as a value object and the poller pulled the existing bin entities in the wardrobe microservice with the href as the unique id. 
The Shoe model in the shoes microservice had the binVO foreign key. The shoe model also included the manufacturer, model name, color, and image url as described in the directions.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

First, I designed a model for hats that has fabric, style name, color, picture url, and location.  The location has a one to many relationship with the hat model which is why a foreign key was created.  When a location is deleted the hats inside should be removed, therefore cascade was the appropriate choice.  

A location value object model was created to work as the receiver when the poller sends in a new location.  The location is directly needed in the hat model to track where it is located.  Without the LocationVO model, the Hat model would not be able to have the location information.  

Within the LocationVO, the import_href was added to link each unique location for the hats.  The closet name, section number, and shelf number from the location model all seem to be necessary information to take in.  
