function HatColumns(props) {
  const handleClick = async hatUrl => {
      const url = "http://localhost:8090" + hatUrl
      const fetchConfig = {method: "delete"}
      console.log(url)
      const response = await fetch(url, fetchConfig)
      if(response.ok) {
          window.location.reload()
          console.log("Successfully deleted hat")
      }
  }
      return (
    <div className="col">
      {props.list.map(data => {
        const hat = data;
        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img src={hat.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title text-center">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted text-center">
                  Closet: {hat.location.closet_name}, Section: {hat.location.section_number}, Shelf: {hat.location.shelf_number}
              </h6>
              <p className="card-text text-center">
                  Fabric: {hat.fabric}, Color: {hat.color}
              </p>
            </div>
            <div className="card-footer text-center" >
              <button onClick={() => { handleClick(hat.href) }}>Delete</button>
            </div>
          </div>
        );
      })}
    </div>
  );
}


export default HatColumns
