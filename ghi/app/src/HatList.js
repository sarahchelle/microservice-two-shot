import React from 'react';
import { Link } from 'react-router-dom';
import HatColumns from './HatColumns';




class HatList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        hatColumns: [[], [], []],
    };
  }



  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {

        const data = await response.json();
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }
        const responses = await Promise.all(requests);
        const hatColumns = [[], [], []];
        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            hatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        this.setState({hatColumns: hatColumns});
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-5 text-center bg-secondary">
          <h1 className="display-5 text-light fw-bold">Hats</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              The only resource you'll ever need to organize and track all of your hats.
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-dark btn-lg px-4 gap-3">Add a new hat to your closet</Link>
            </div>
            <img className="bg-white rounded shadow d-block mt-5 mb-5 mx-auto mb-4" src="https://i.pinimg.com/564x/79/46/74/794674f2c4af0694a8e4bfeb3f6f4ba6.jpg" alt="" width="600" />
          </div>
        
        <div className="container text-light">
          <h2 className="mt-5">Hat Collection</h2>
          <div className="row mt-5">
            {this.state.hatColumns.map((hatList, index) => {
              return (
                <HatColumns key={index} list={hatList} />
              );
            })}
          </div>
        </div>
        </div>
      </>
    );
  }
}

export default HatList