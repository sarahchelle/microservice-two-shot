function ShoeColumns(props) {
    const handleClick = async shoeUrl => {
        const url = "http://localhost:8080" + shoeUrl
        const fetchConfig = {method: "delete"}
        const response = await fetch(url, fetchConfig)
        if(response.ok) {
            window.location.reload()
        }
    }
        return (
      <div className="col">
        {props.list.map(data => {
          const shoe = data;
          return (
            <div key={shoe.href} className="card mb-3 shadow">
              <img src={shoe.picture_url} className="card-img-top" />
              <div className="card-body">
                <h5 className="card-title text-center">{shoe.manufacturer} {shoe.model_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted text-center">
                    Closet: {shoe.bin.closet_name}, Section: {shoe.bin.bin_number}
                </h6>
                <p className="card-text text-center">
                    Color: {shoe.color}
                </p>
              </div>
              <div className="card-footer text-center" >
                <button onClick={() => { handleClick(shoe.href) }}>Delete</button>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
  
  
  export default ShoeColumns
  